function issue1003(input: string) {
    if (Number(input.trim()) % 6 == 0) {
        console.log('Yes');
    } else {
        console.log('No');
    }
}

switch (process.platform) {
    case "win32":
        issue1003(require('fs').readFileSync('./stdin.txt', 'utf8'));
        break;
    default:
        issue1003(require('fs').readFileSync('/dev/stdin', 'utf8'));
        break;
}
