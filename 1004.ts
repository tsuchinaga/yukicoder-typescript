function issue1004(input: string) {
    let inputs = input.trim().split(" ").map((a: string): number => Number(a))
    let x0 = inputs[2]
    let n = inputs[3]

    // 奇数と偶数が繰り返されるため、最初が奇数なら一人目が、最初が偶数なら二人目が勝つ
    let w = Math.floor(n / 2)
    if (x0 % 2 == 1) console.log([w, 0].join(" "))
    else console.log([0, w].join(" "))
}

let path: string = "/dev/stdin"
if (process.platform == "win32") path = "./stdin.txt"
issue1004(require('fs').readFileSync(path, 'utf8'))
