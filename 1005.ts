function issue1005(input: string) {
    let inputs = input.trim().split("\n")
    let s: string = inputs[0]
    let t: string = inputs[1]

    // 先頭からマッチするところを探して、マッチしたらマッチした最後の文字の前に.を入れ、マッチした文字の最後の文字から確認を再開する
    let count = 0
    if (t.length == 1 && s.includes(t)) count = -1
    else {
        let head = 0
        while (head <= s.length - t.length) {
            if (s.substr(head, t.length) == t) {
                count++
                head += t.length - 1
            } else head++
        }
    }
    console.log(count)
}

let path: string = "/dev/stdin"
if (process.platform == "win32") path = "./stdin.txt"
issue1005(require('fs').readFileSync(path, 'utf8'))
