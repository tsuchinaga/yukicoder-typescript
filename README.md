# yukicoder/typescript

yukicoderの問題をtypescriptで解いてみる

## 環境

* Windows 10
* node v12.18.3
* npm 6.14.6
* typescript 4.0.3

## 実行

stdin.txtをパッケージ直下に作って、入力を書き込んで保存します。  
このとき改行コードは `\n` で、最後の1行の後にも改行をいれて、最終行にEOFがくるようにする。

あとは実行したいtsファイルを引数にいれてexecをたたく  
`$ npm run exec -- 1003.ts`
